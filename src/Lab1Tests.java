import static org.junit.Assert.*;

import org.junit.Test;


public class Lab1Tests {

	/**
	 * Test esistenza classe linked list:
	 * Deve esistere una classe chiamata LinkedList.
	 */
	@Test
	public void test1() {
		try{
			Class.forName("LinkedList");
		}catch(ClassNotFoundException e){
			fail("LinkedList not yet implemented");
		}
	}
	
	/**
	 * Test istanziazione linked list vuota:
	 * Il costruttore di default deve inizializzare una lista vuota.
	 * La descrizione della lista vuota deve essere "[]".
	 */
	@Test
	public void test2() {
		LinkedList l = new LinkedList();
		assertEquals("[]", l.toString());
	}
	
	/**
	 * Test esistenza costruttore linked list da stringa:
	 * Deve esistere un costruttore che prende come parametro un oggetto di tipo String.
	 */
	@Test
	public void test3() {
		try{
			LinkedList.class.getConstructor(java.lang.String.class);
		}catch(NoSuchMethodException e){
			fail("Contructor not yet implemented");
		}
	}
	
	/**
	 * Test istanziazione linked list da stringa:
	 * Il costruttore da String inizializza una lista con gli elementi specificati.
	 * Ogni elemento (numero reale) e' separato da uno spazio.
	 */
	@Test
	public void test4() {
		LinkedList l = new LinkedList("1 2 3");
		assertEquals("[1.0 2.0 3.0]", l.toString());
		l = new LinkedList("1.23 2.001 3.75");
		assertEquals("[1.23 2.001 3.75]", l.toString());
		l = new LinkedList("");
		assertEquals("[]", l.toString());
		
	}

}
